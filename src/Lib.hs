module Lib
  ( withVertexAttribArray

  , drawQuads
  , drawTrigs
  , drawPrimArray2d

  , quad
  , ToQuad(..)

  , glFloat
  , debugM
  ) where

import Control.Monad.IO.Class (MonadIO(..))
import Data.StateVar (($=))
import Data.Vector.Storable (Vector)
import Debug.Trace (traceM)
import Linear (V2(..), V4(..))
import Unsafe.Coerce (unsafeCoerce)

import qualified Data.Vector.Storable as Vector
import qualified Graphics.Rendering.OpenGL as GL

withVertexAttribArray :: MonadIO m => GL.AttribLocation -> Vector Float -> m () -> m ()
withVertexAttribArray location vertices action = do
  GL.vertexAttribArray location $= GL.Enabled

  liftIO . Vector.unsafeWith vertices $ \ptr ->
    GL.vertexAttribPointer location $=
      ( GL.ToFloat
      , GL.VertexArrayDescriptor 2 GL.Float 0 ptr
      )

  action

  GL.vertexAttribArray location $= GL.Disabled

quad :: Float -> Float -> Float -> Float -> Vector Float
quad width height tx ty = Vector.fromList
  [ tx - width * 0.5, ty - height * 0.5
  , tx - width * 0.5, ty + height * 0.5
  , tx + width * 0.5, ty + height * 0.5
  , tx + width * 0.5, ty - height * 0.5
  ]

class ToQuad a where
  toQuad :: a -> Vector Float

instance ToQuad (V4 Float) where
  toQuad (V4 width height tx ty) = quad width height tx ty
  {-# INLINE toQuad #-}

instance ToQuad (V2 Float) where
  toQuad (V2 width height) = quad width height 0 0
  {-# INLINE toQuad #-}

instance ToQuad (Float, Float, Float, Float) where
  toQuad (width, height, tx, ty) = quad width height tx ty
  {-# INLINE toQuad #-}

instance ToQuad (Float, Float) where
  toQuad (width, height) = quad width height 0 0
  {-# INLINE toQuad #-}

drawQuads :: MonadIO m => GL.AttribLocation -> Vector Float -> m ()
drawQuads = drawPrimArray2d GL.Quads

drawTrigs :: MonadIO m => GL.AttribLocation -> Vector Float -> m ()
drawTrigs = drawPrimArray2d GL.Triangles

drawPrimArray2d :: MonadIO m => GL.PrimitiveMode -> GL.AttribLocation -> Vector Float -> m ()
drawPrimArray2d primitive coord2d vertices =
  withVertexAttribArray coord2d vertices $
    liftIO $ GL.drawArrays primitive 0 numVertices
  where
    numVertices = fromIntegral $ Vector.length vertices `div` 2
{-# INLINE drawPrimArray2d #-}

glFloat :: Float -> GL.GLfloat
glFloat x = unsafeCoerce x
{-# INLINE glFloat #-}

-- | Pin output type re-export qualified.
debugM :: Monad m => String -> m ()
debugM = traceM
{-# INLINE debugM #-}
