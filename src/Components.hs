{-# LANGUAGE TemplateHaskell #-}

module Components
  ( World
  , initWorld

  , Position(..)
  ) where

import Apecs
import Apecs.TH
import Linear (V2)

import Components.Textures (Textures)
import Components.Programs (Programs)

newtype Position = Position
  { unPosition :: V2 Float
  }

makeMapComponents
  [ ''Position
  ]

makeWorld "World"
  [ ''Textures
  , ''Programs

  , ''Position
  ]
