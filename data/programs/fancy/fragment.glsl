#define TAU 6.28318530718

uniform float u_time;
uniform vec2  u_resolution;
uniform vec2  u_size;
uniform vec2  u_offset;

varying vec2 f_texcoord;

void main() {
  // Screen-space:
  // vec2 pos = gl_FragCoord.xy / u_resolution;

  // Texure-space:
  // vec2 pos = f_texcoord;

  // Texure-space, squared:
  vec2 pos2 = f_texcoord;
  if (u_size.x > u_size.y) {
    pos2.y /= u_size.y / u_size.x;
    pos2.y -= 0.25;
  } else
  if (u_size.y > u_size.x) {
    pos2.x /= u_size.x / u_size.y;
    pos2.x -= 0.25;
  }

  // Polar from square-space:
  vec2 posP = vec2(0.5 - pos2);
  float r = length(posP) * 2.0;
  float a = atan(posP.y, posP.x) + u_time;
  float f = smoothstep(-1.0, 1.0, cos(a * 10.0)) * 0.33 + 0.5;
  float s = 1.0 - smoothstep(f, f + 0.02, r);

  vec3 color = vec3(pos2, 0);

  vec2 dist = pos2 - vec2(0.5);
  float radius = 0.9;
  float alpha = smoothstep(
    radius - radius * 0.01,
    radius,
    dot(dist, dist) * 4.0
  );

	gl_FragColor = vec4(color, s);
}
