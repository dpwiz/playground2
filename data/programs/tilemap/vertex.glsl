attribute vec2 position;
attribute vec2 texture;

// uniform mat4 mvp;

uniform vec2 viewOffset;
uniform vec2 viewportSize;

uniform vec2 inverseTileTextureSize;
uniform float inverseTileSize;

varying vec2 pixelCoord;
varying vec2 texCoord;

void main(void) {
  pixelCoord = (texture * viewportSize) + viewOffset;
  texCoord = pixelCoord * inverseTileTextureSize * inverseTileSize;
  gl_Position
    = gl_ProjectionMatrix
    * gl_ModelViewMatrix
    * vec4(position, 0.0, 1.0);
}
